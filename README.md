# Set up environment

## Automatic
1. Run docker-compose inside the solution directory.

> docker-compose up --build -d

2. Open two browser windows and start chatting (https://localhost:80)

## Manual

1. Start SQLServer

> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -e 'MSSQL_PID=Express' -p 1433:1433 --name chat_challenge_sql -h chat_challenge_sql -d mcr.microsoft.com/mssql/server:2019-latest

2. Update WebApp\appsettings.json 
- Change 'RabbitMQ:Server' to 'localhost'; 
- Change server name' in 'DefaultConnection' from 'chat_challenge_sql' to 'localhost'

3. Start RabbitMQ

> docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management

4. Update StockProcessorService\appsettings.json and change 'RabbitMQ:Server' to 'localhost'; 

5. Update database

In Visual Studio, you can use the Package Manager Console to apply pending migrations to the database:
> PM > cd WebApp

> PM > Update-Database

Alternatively, you can apply pending migrations from a command prompt at your project directory:
> cd WebApp

> dotnet ef database update

6. Running the application

- Debug StockProcessorService in Visual Studio
- Debug WebApp in Visual Studio
- Open two browser windows and start chatting (https://localhost:80)

# Technologies and Tools
- EF Core
- RabbitMQ
- ASP.Net Identity
- SignalR
- ASP.Net Core
- Docker

