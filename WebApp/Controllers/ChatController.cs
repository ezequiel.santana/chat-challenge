﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Service;

namespace WebApp.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;

        public ChatController([FromServices] IChatService chatService)
        {
            _chatService = chatService;
        }

        public IActionResult Index()
        {
            List<ChatMessage> lastMessages = _chatService.GetLastMessages();
            return View(lastMessages);
        }        

        [HttpPost]
        public async Task<IActionResult> PostMessage([FromBody] PostRequest postRequest)
        {
            await _chatService.PostMessage(User.Identity.Name, postRequest.Message);

            return Ok();
        }

    }
}
