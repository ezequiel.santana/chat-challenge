﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Shared;
using System;

namespace WebApp.Service
{
    public class StockResponseMonitor
    {
        public StockResponseMonitor(ILogger<StockResponseMonitor> logger, IHubContext<ChatHub> chatHub, IConfiguration configuration)
        {
            var factory = new ConnectionFactory() 
            {
                HostName = configuration.GetValue<String>("RabbitMQ:Server"),
            };
            var connection = factory.CreateConnection();

            var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "StockResponses",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();

                StockResponseDto response = StockResponseDto.FromUtf8Bytes(body);

                logger.LogInformation(" Received a response for {0}", response.Response);

                chatHub.Clients.User(response.To).SendAsync("ReceiveMessage", response.From, response.Response);
            };

            channel.BasicConsume(queue: "StockResponses",
                                         autoAck: true,
                                         consumer: consumer);

        }
    }
}
