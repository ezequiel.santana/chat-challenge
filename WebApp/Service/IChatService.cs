﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Service
{
    public interface IChatService {
        public Task PostMessage(String author, String message);

        public List<ChatMessage> GetLastMessages();
    }
}
