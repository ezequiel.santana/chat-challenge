﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using Shared;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Service
{
    public class ChatService: IChatService
    {
        private readonly IHubContext<ChatHub> _chatHub;
        private readonly IChatRepository _chatRepository;
        private readonly IConfiguration _configuration;

        public ChatService(IChatRepository chatRepository, IHubContext<ChatHub> chatHub, IConfiguration configuration)
        {
            _chatRepository = chatRepository;
            _chatHub = chatHub;
            _configuration = configuration;
        }

        public async Task PostMessage(String author, String message)
        {
            string stockPattern = "(\\/stock=)(.+)";
            Regex rg = new Regex(stockPattern);
            Match match = rg.Match(message);

            if (match.Success)
            {                
                String stock = match.Groups[2].Value;
                await _chatHub.Clients.User(author).SendAsync("ReceiveMessage", "BOT", String.Format("{0}, your request for {1} is being processed. Just wait a litle bit.", author, stock));

                StockRequestDto request = new StockRequestDto()
                {
                    From = author,
                    Query = stock
                }; 

                await Task.Run(() => PostStockRequest(request));
            }
            else
            {
                await _chatHub.Clients.All.SendAsync("ReceiveMessage", author, message);

                _chatRepository.SaveMessage(author, message);
            }
        }

        public List<ChatMessage> GetLastMessages()
        {
            int count = _configuration.GetValue<int>("LastMessagesToRetrieve");
            return _chatRepository.GetLastMessages(count);
        }

        private void PostStockRequest(StockRequestDto request)
        {
            var factory = new ConnectionFactory() 
            {
                HostName = _configuration.GetValue<String>("RabbitMQ:Server"),
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "StockRequests",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = request.ToUtf8Bytes();

                channel.BasicPublish(exchange: "",
                                     routingKey: "StockRequests",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}
