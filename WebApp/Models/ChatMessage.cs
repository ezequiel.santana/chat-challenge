﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class ChatMessage
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
    }
}
