﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Repository
{
    public class ChatRepository: IChatRepository
    {
        private readonly ApplicationDbContext _context;

        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SaveMessage(String author, String message)
        {
            var record = new ChatMessage
            {
                Text = message,
                Author = author,
                Timestamp = DateTime.Now
            };

            _context.ChatMessages.Add(record);
            _context.SaveChanges();
        }

        public List<ChatMessage> GetLastMessages(int count)
        {
            var messages = from m in _context.ChatMessages
                           orderby m.Timestamp descending
                           select m;

            return messages.Take(count).OrderBy(m => m.Timestamp).ToList();
        }

    }
}
