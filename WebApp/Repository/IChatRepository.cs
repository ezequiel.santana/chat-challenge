﻿using System;
using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.Repository
{
    public interface IChatRepository
    {
        public void SaveMessage(String author, String message);

        public List<ChatMessage> GetLastMessages(int count);
    }
}
