using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StockProcessorService.Service;

namespace StockProcessorService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<StockProcessor>();
                    services.AddHostedService<Worker>();
                });
    }
}
