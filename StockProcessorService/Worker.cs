using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Shared;
using StockProcessorService.Model;
using StockProcessorService.Service;

namespace StockProcessorService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private IConnection _connection;
        private readonly StockProcessor _stockProcessor;
        private readonly IConfiguration _configuration;

        public Worker(ILogger<Worker> logger, StockProcessor stockProcessor, IConfiguration configuration)
        {
            _logger = logger;

            _stockProcessor = stockProcessor;

            _configuration = configuration;

            CreateConnection();

            ConfigureConsumer();
        }

        private void ConfigureConsumer()
        {
            var channel = _connection.CreateModel();

            channel.QueueDeclare(queue: "StockRequests",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();                
                StockRequestDto request = StockRequestDto.FromUtf8Bytes(body);
                Console.WriteLine(" Received a request to query for {0}", request);

                StockResponseDto response = _stockProcessor.ProcessStockRequest(request);
                SendResponse(response);                
            };

            channel.BasicConsume(queue: "StockRequests",
                                 autoAck: true,
                                 consumer: consumer);
        }

        private void CreateConnection()
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _configuration.GetValue<String>("RabbitMQ:Server"),
                };
                _connection = factory.CreateConnection();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Could not create connection: {ex.Message}");
            }
        }       

        private void CheckConnection()
        {
            if (_connection != null)
            {
                CreateConnection();
            }
        }

        private void SendResponse(StockResponseDto response)
        {            
            CheckConnection();
            using (var channel = _connection.CreateModel())
            {

                channel.QueueDeclare(queue: "StockResponses",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var sendResults = response.ToUtf8Bytes();

                channel.BasicPublish(exchange: "",
                                     routingKey: "StockResponses",
                                     basicProperties: null,
                                     body: sendResults);
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
