﻿using CsvHelper.Configuration.Attributes;
using System;

namespace StockProcessorService.Model
{
    public class Stock
    {
        [Name("Symbol")]
        public String Symbol { get; set; }
        [Name("Date")]
        public String Date { get; set; }
        [Name("Time")]
        public String Time { get; set; }
        [Name("Open")]
        public String Open { get; set; }
        [Name("High")]
        public String High { get; set; }
        [Name("Low")]
        public String Low { get; set; }
        [Name("Close")]
        public String Close { get; set; }
        [Name("Volume")]
        public String Volume { get; set; }

        public override string ToString()
        {
            return String.Format("Symbol: {0}, Date: {1}, Time: {2}, Open: {3}, Hight: {4}, Low: {5},  Close: {6}, Volume: {7}", Symbol, Date, Time, Open, High, Low, Close, Volume);
        }
    }
}
