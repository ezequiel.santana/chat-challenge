﻿using CsvHelper;
using Microsoft.Extensions.Logging;
using Shared;
using StockProcessorService.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StockProcessorService.Service
{
    public class StockProcessor
    {
        private readonly ILogger<StockProcessor> _logger;

        public StockProcessor(ILogger<StockProcessor> logger)
        {
            _logger = logger;
        }

        public StockResponseDto ProcessStockRequest(StockRequestDto request)
        {
            StockResponseDto response = new StockResponseDto() { From = "BOT", To = request.From };

            Stream str = null;
            try
            {
                string url = "https://stooq.com/q/l/?s=" + request.Query + "&f=sd2t2ohlcv&h&e=csv";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                str = resp.GetResponseStream();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error getting {0}. {1}.", request.Query, ex.Message);

                response.Response = String.Format("Sorry {0}, I couldn't get a correct answer from https://stooq.com. Stock code '{1}'.", request.From, request.Query);

                return response;
            }

            response = ProcessStockRequest(request, response.From, response.To, str);

            return response;
        }

        public StockResponseDto ProcessStockRequest(StockRequestDto request, string from, string to, Stream str)
        {
            StockResponseDto response = new StockResponseDto() { From = from, To = to };

            List<Stock> records = null;
            try
            {
                using (var reader = new StreamReader(str))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    records = csv.GetRecords<Stock>().ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error parsing CSV for stock {0}. {1}.", request.Query, ex.Message);

                response.Response = String.Format("Sorry {0}, I couldn't parse the CSV from https://stooq.com. Stock code '{1}'.", request.From, request.Query);

                return response;
            }

            if (records != null && records.Count == 1)
            {
                Stock stock = records.First();

                if (stock.Date.Equals("N/D") && stock.Low.Equals("N/D") && stock.High.Equals("N/D") && stock.Close.Equals("N/D"))
                {
                    response.Response = String.Format("Sorry {0}, are you sure that the stock code '{1}' is valid?", request.From, request.Query);
                }
                else
                {
                    //String text = String.Format("Stock: {0}, Date: {1}, Low: {2},  High: {3}, Close: {4}", stock.Symbol, stock.Date, stock.Low, stock.High, stock.Close);
                    String text = String.Format("APPL.US quote is ${0} per share", stock.Close);
                    response.Response = text;
                }
            }
            else
            {
                response.Response = String.Format("Sorry {0}, I couldn't understand the answer from https://stooq.com. Stock code '{1}'.", request.From, request.Query);
            }
            return response;
        }

    }
}
