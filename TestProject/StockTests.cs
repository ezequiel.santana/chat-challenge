using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Shared;
using StockProcessorService.Service;
using Xunit;

namespace TestProject
{
    public class StockTests
    {
        private readonly StockProcessor _stockProcessor;

        public StockTests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();

            var factory = serviceProvider.GetService<ILoggerFactory>();

            ILogger<StockProcessor> logger = factory.CreateLogger<StockProcessor>();

            _stockProcessor = new StockProcessor(logger);
        }

        [Fact]
        public void WithCorrectQuery()
        {
            StockRequestDto request = new StockRequestDto();
            request.From = "ezequiel.santana@gmail.com";
            request.Query = "aapl.us";

            StockResponseDto response = _stockProcessor.ProcessStockRequest(request);
            Assert.Contains("Stock: AAPL.US", response.Response);
        }

        [Fact]
        public void WithWrongQuery()
        {
            StockRequestDto request = new StockRequestDto() { From = "ezequiel.santana@gmail.com", Query = "aapl .us" };

            StockResponseDto response = _stockProcessor.ProcessStockRequest(request);
            Assert.Contains("I couldn't understand the answer from", response.Response);
        }

        [Fact]
        public void WithNoReturn()
        {
            StockRequestDto request = new StockRequestDto() { From = "ezequiel.santana@gmail.com", Query = "fake" };

            StockResponseDto response = _stockProcessor.ProcessStockRequest(request);
            Assert.Contains("Sorry ezequiel.santana@gmail.com, are you sure that the stock code 'fake' is valid?", response.Response);
        }
    }
}
