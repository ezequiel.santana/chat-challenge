﻿using System;
using System.Text.Json;

namespace Shared
{
    public class StockRequestDto
    {
        public String From { get; set; }
        public String Query { get; set; }

        public byte[] ToUtf8Bytes()
        {
            return JsonSerializer.SerializeToUtf8Bytes(this);
        }

        public static StockRequestDto FromUtf8Bytes(byte[] jsonUtf8Bytes)
        {
            var utf8Reader = new Utf8JsonReader(jsonUtf8Bytes);
            StockRequestDto dto = JsonSerializer.Deserialize<StockRequestDto>(ref utf8Reader);

            return dto;
        }

        public override string ToString()
        {
            return string.Format("From: {0}, Query: {1}", From, Query);
        }
    }
}
