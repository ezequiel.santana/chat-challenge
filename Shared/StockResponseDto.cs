﻿using System;
using System.Text.Json;

namespace Shared
{
    public class StockResponseDto
    {
        public String From { get; set; }

        public String To { get; set; }

        public String Response { get; set; }

        public byte[] ToUtf8Bytes()
        {
            return JsonSerializer.SerializeToUtf8Bytes(this);
        }

        public static StockResponseDto FromUtf8Bytes(byte[] jsonUtf8Bytes)
        {
            var utf8Reader = new Utf8JsonReader(jsonUtf8Bytes);
            StockResponseDto dto = JsonSerializer.Deserialize<StockResponseDto>(ref utf8Reader);

            return dto;
        }

        public override string ToString()
        {
            return String.Format("From: {0}, To: {1}, Response: {2}", From, To, Response);
        }
    }
}
